# Frameworks:
- webdriverIO (v6)
- cucumber (v6)

# Features:
- Supports Page Object Model
- Compatible with Nodejs Versions 8.x to 13.x
- Contains sample Scenarios written in Declarative style of BDD
- Supports Data externalisation
- Integrated with [eslint](https://www.npmjs.com/package/eslint) for identifying and reporting on code patterns.
- Integrated with [cucumber-html-reporter](https://www.npmjs.com/package/cucumber-html-reporter) for intuitive & detailed HTML reporting
- Embeds screenshots on failure
- Integrated with [wdio-cucumber-parallel-execution](https://www.npmjs.com/package/wdio-cucumber-parallel-execution) module for parallel execution


# Installation

```
npm install
npm install @wdio/devtools-service --save-dev
```


# Running the test with configurations as specified in conf.js

```
npm run test
```

# Write new test
1. First write your BDD (cucumber) test in the correct feature file (spec) located in ./tests/features/featureFiles
    If the correct feature file doesn't exist, you create a new one
2. Write the "Glue code" in the correct step definition file located in ./tests/features/step_definitions
3. Methods should be defined on page level located in ./test/pages
4. If you cannot build your locators intuitive, they should be defined in ./test/locators
5. Other constants can be defined in ./tests/files/testData

# Reporting
Html reports can be found in ./tests/reports/html (i've added reports from 2 runs)

# GitLab CI/CD integration
I built the steps and methods on the DevTools service. When I tried to setup the pipeline job I found out that Devtools is not 
supported in cloud applications. It was too late to change service used. 

I tried selenium-standalone and chromedriver but those also did not work on the pipeline job. 
I might have been able to resolve those issues in the yml file but;
If I had chosen to use those services I would've had to change some methods so that they would work correctly with the new service, but I did not have any time left.

Above step can still be done but will need some more time and effort.