const { Given, When, Then } = require('cucumber');

const businessShowroomPage = require('../../pages/businessShowroom.page.js');
const businessShowroom = new businessShowroomPage();


Given(/^I am on the Business Showroom page$/, () => {
    businessShowroom.openBusinessShowroomPage();
});

When(/^I consent to cookies$/, () => {
    businessShowroom.consentCookies();
});