const { Given, When, Then } = require('cucumber');

const businessShowroomPage = require('../../pages/businessShowroom.page.js');
const businessShowroom = new businessShowroomPage();


When(/^I open the (.*) filterbutton$/, (filterKey) => {
    businessShowroom.openDesktopFilter(filterKey);
});

When(/^I select the (.*) option$/, (filterKey) => {
    businessShowroom.optionDesktopFilter(filterKey);
});

When(/^I select filter (.*) in the (.*) checkbox list$/, (checkboxValue, checkboxName) => {
    businessShowroom.selectCheckbox(checkboxValue, checkboxName);
});

When(/^I save filter settings$/, () => {
    businessShowroom.saveFilterSetting();
});

When(/^I set monthly price from (.*) to (.*) and term amount to (.*) months and kilometrage amount to (.*)$/, (minMonthlyPrice, maxMonthlyPrice, termAmount, kilometrageAmount) => {
    businessShowroom.setMonthlyPriceAndLeaseOption(minMonthlyPrice, maxMonthlyPrice, termAmount, kilometrageAmount);
});

Then(/^I should get (.*) message/, (partialMessage) => {
    businessShowroom.validateNoResultsFoundMessage(partialMessage);
});

Then(/^I should see (.*) text above the grid/, (partialText) => {
    businessShowroom.validateTextFoundMessage(partialText);
});

Then(/^I should see a (.*) in the results grid/, (makeModel) => {
    businessShowroom.validateMakeModelDisplayed(makeModel);
});

Then(/^I see a monthly price of (.*) in the results grid/, (monthlyPrice) => {
    businessShowroom.validatePriceDisplayed(monthlyPrice);
});



