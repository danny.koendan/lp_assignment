Feature: Validate the desktopfilter functionalities on the showroom page
  This feature will test all the filterbuttons and some of their options
  It will also validate some of the prices based on contract settings (20-10-2020)

  Background:
    Given I am on the Business Showroom page

  @smoke
  Scenario: User should be able to open business showroom page
    When I consent to cookies
    Then I should see At LeasePlan we offer a very wide range text above the grid

  Scenario Outline: User should be able to get results from the popular filters searches
    When I open the popularFilters filterbutton
    And I select filter <checkboxOption> in the "popularFilter" checkbox list
    And I save filter settings
    Then I should see a <makeModel> in the results grid
      Examples:
      | checkboxOption | makeModel |
      | Best deals | Polestar 2 |
      | Configure yourself | Volvo Xc40 |

  Scenario Outline: User should be able to get results from make and model searches
    When I open the makemodel filterbutton
    And I select filter <make> in the "make" checkbox list
    And I select filter <model> in the "model" checkbox list
    And I save filter settings
    Then I should see a <makeModel> in the results grid
    Examples:
      | make | model | makeModel |
      | AUDI | A1 | Audi A1      |
      | MERCEDES | CLA | Mercedes Cla |
      | BMW      | 1   | BMW 1        |
      | SKODA    | CITIGO | Skoda Citigo |
      | TESLA    | S      | Tesla S      |
      | VOLKSWAGEN | ARTEON | Volkswagen Arteon |
      | VOLVO      | S90    | Volvo S90         |

  Scenario Outline: User should be able to see the correct prices based on selected lease options
    When I set monthly price from <minPrice> to <maxPrice> and term amount to <months> months and kilometrage amount to <kilometers>
    Then I should see a <makeModel> in the results grid
    And I see a monthly price of <price> in the results grid
    Examples:
      # Monthly price should be set between 200 and 3890 in steps of 5
      # Term should be placed in amount of 24 36 48 or 60
      # Kilometrage should be set to amounts of 10.000 to 60.000 in steps of 5.000
      | minPrice | maxPrice | months | kilometers | makeModel | price |
      | 705      | 2395     | 48     | 50000      | Volkswagen Passat | € 706 |
      | 1000     | 2395     | 48     | 50000      | BMW 3     | € 1.111 |
      | 1600     | 2395     | 48     | 50000      | Audi A6   | € 1.605 |
      | 1600     | 2395     | 36     | 50000      | Mercedes Cla   | € 1.614 |
      | 675      | 2395     | 24     | 50000      | Volvo Xc40   | € 713 |
      | 675      | 2395     | 24     | 10000      | Volvo Xc40   | € 675 |

  Scenario: User should get no results when selecting wrong price and lease options combinations
    And I set monthly price from 250 to 1200 and term amount to 60 months and kilometrage amount to 60000
    Then I should get try again message

  Scenario Outline: User should be able to get results from fuel type searches
    When I open the fuelTypes filterbutton
    And I select filter <fuelType>" in the "fuelType" checkbox list
    And I save filter settings
    Then I should see a <makeModel> in the results grid
    Examples:
      | fuelType | makeModel |
      | CNG      | Audi A3   |
      | Diesel   | Volkswagen Passat |
      | Electric | Polestar 2        |
      | Hybrid   | Volvo V60         |
      | LPG      | Dacia Duster      |
      | Petrol   | Audi A3           |

  Scenario Outline: User should be able to get results from body type searches via the more filters option
    When I select the moreFilters option
    And I select filter <bodyType>" in the "bodyType" checkbox list
    And I save filter settings
    Then I should see a <makeModel> in the results grid
    Examples:
      | bodyType | makeModel |
      | CONVERTIBLE | Volkswagen T-Roc |
      | MINIBUS     | Renault Kangoo   |
      | SEDAN       | Polestar 2       |
      | COUPE       | BMW 4            |
      | MINIVAN     | BMW 2 Gran Tourer |
      | SUV         | Volvo Xc60       |
      | HATCHBACK   | Audi A3          |
      | WAGON       | Volvo V60        |
      | MINI MPV    | Kia Soul         |

  Scenario Outline: User should be able to get results from transmission type searches via the more filters option
    When I select the moreFilters option
    And I select filter <transmissionType>" in the "transmission" checkbox list
    And I save filter settings
    Then I should see a <makeModel> in the results grid
    Examples:
      | transmissionType | makeModel |
      | Automatic | Polestar 2 |
      | Manual    | Volkswagen Tiguan |

  Scenario: User should be able to reset all filters
    When I open the makemodel filterbutton
    And I select filter "VOLVO" in the "make" checkbox list
    And I select filter "S60" in the "model" checkbox list
    And I save filter settings
    And I select the resetFilters option
    Then I should see a Audi A3 in the results grid

  Scenario: User should get relevant results on using several filters at the time
    And I set monthly price from 800 to 1200 and term amount to 36 months and kilometrage amount to 55000
    When I open the popularFilters filterbutton
    And I select filter "Configure yourself" in the "popularFilter" checkbox list
    And I save filter settings
    And I select the resetFilters option

    And I open the makemodel filterbutton
    And I select filter "AUDI" in the "make" checkbox list
    And I select filter "A1" in the "model" checkbox list
    And I save filter settings

    And I open the monthlyPrice filterbutton
    And I save filter settings

    And I open the leaseOption filterbutton
    And I save filter settings

    And I open the fuelTypes filterbutton
    And I select filter "Petrol" in the "fuelType" checkbox list
    And I save filter settings

    And I select the moreFilters option
    And I select filter "HATCHBACK" in the "bodyType" checkbox list
    And I save filter settings

    And I select the moreFilters option
    And I select filter "Automatic" in the "transmission" checkbox list
    And I save filter settings

    Then I should see a Audi A1 in the results grid
