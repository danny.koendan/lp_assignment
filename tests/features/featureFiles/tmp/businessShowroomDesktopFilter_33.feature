Feature: Validate the desktopfilter functionalities on the showroom page
Scenario Outline: User should be able to get results from transmission type searches via the more filters option
Given I am on the Business Showroom page
When I select the moreFilters option
And I select filter <transmissionType>" in the "transmission" checkbox list
And I save filter settings
Then I should see a <makeModel> in the results grid
Examples:
|transmissionType|makeModel|
|Automatic|Polestar 2|
