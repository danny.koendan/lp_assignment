Feature: Validate the desktopfilter functionalities on the showroom page
Scenario: User should be able to open business showroom page
Given I am on the Business Showroom page
When I consent to cookies
Then I should see At LeasePlan we offer a very wide range text above the grid
