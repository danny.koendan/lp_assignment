Feature: Validate the desktopfilter functionalities on the showroom page
Scenario Outline: User should be able to get results from make and model searches
Given I am on the Business Showroom page
When I open the makemodel filterbutton
And I select filter <make> in the "make" checkbox list
And I select filter <model> in the "model" checkbox list
And I save filter settings
Then I should see a <makeModel> in the results grid
Examples:
|make|model|makeModel|
|TESLA|S|Tesla S|
