Feature: Validate the desktopfilter functionalities on the showroom page
Scenario Outline: User should be able to see the correct prices based on selected lease options
Given I am on the Business Showroom page
When I set monthly price from <minPrice> to <maxPrice> and term amount to <months> months and kilometrage amount to <kilometers>
Then I should see a <makeModel> in the results grid
And I see a monthly price of <price> in the results grid
Examples:
|minPrice|maxPrice|months|kilometers|makeModel|price|
|675|2395|24|50000|Volvo Xc40|€ 713|
