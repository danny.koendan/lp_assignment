Feature: Validate the desktopfilter functionalities on the showroom page
Scenario Outline: User should be able to get results from the popular filters searches
Given I am on the Business Showroom page
When I open the popularFilters filterbutton
And I select filter <checkboxOption> in the "popularFilter" checkbox list
And I save filter settings
Then I should see a <makeModel> in the results grid
Examples:
|checkboxOption|makeModel|
|Best deals|Polestar 2|
