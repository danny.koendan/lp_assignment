Feature: Validate the desktopfilter functionalities on the showroom page
Scenario: User should get relevant results on using several filters at the time
Given I am on the Business Showroom page
And I set monthly price from 800 to 1200 and term amount to 36 months and kilometrage amount to 55000
When I open the popularFilters filterbutton
And I select filter "Configure yourself" in the "popularFilter" checkbox list
And I save filter settings
And I select the resetFilters option
And I open the makemodel filterbutton
And I select filter "AUDI" in the "make" checkbox list
And I select filter "A1" in the "model" checkbox list
And I save filter settings
And I open the monthlyPrice filterbutton
And I save filter settings
And I open the leaseOption filterbutton
And I save filter settings
And I open the fuelTypes filterbutton
And I select filter "Petrol" in the "fuelType" checkbox list
And I save filter settings
And I select the moreFilters option
And I select filter "HATCHBACK" in the "bodyType" checkbox list
And I save filter settings
And I select the moreFilters option
And I select filter "Automatic" in the "transmission" checkbox list
And I save filter settings
Then I should see a Audi A1 in the results grid
