Feature: Validate the desktopfilter functionalities on the showroom page
Scenario Outline: User should be able to get results from fuel type searches
Given I am on the Business Showroom page
When I open the fuelTypes filterbutton
And I select filter <fuelType>" in the "fuelType" checkbox list
And I save filter settings
Then I should see a <makeModel> in the results grid
Examples:
|fuelType|makeModel|
|Hybrid|Volvo V60|
