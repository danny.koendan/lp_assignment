Feature: Validate the desktopfilter functionalities on the showroom page
Scenario: User should get no results when selecting wrong price and lease options combinations
Given I am on the Business Showroom page
And I set monthly price from 250 to 1200 and term amount to 60 months and kilometrage amount to 60000
Then I should get try again message
