Feature: Validate the desktopfilter functionalities on the showroom page
Scenario Outline: User should be able to get results from body type searches via the more filters option
Given I am on the Business Showroom page
When I select the moreFilters option
And I select filter <bodyType>" in the "bodyType" checkbox list
And I save filter settings
Then I should see a <makeModel> in the results grid
Examples:
|bodyType|makeModel|
|COUPE|BMW 4|
