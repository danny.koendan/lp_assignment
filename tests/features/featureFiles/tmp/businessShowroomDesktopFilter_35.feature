Feature: Validate the desktopfilter functionalities on the showroom page
Scenario: User should be able to reset all filters
Given I am on the Business Showroom page
When I set monthly price from 800 to 1200 and term amount to 36 months and kilometrage amount to 55000
And I open the makemodel filterbutton
And I select filter "Volvo" in the "make" checkbox list
And I select filter "S60" in the "model" checkbox list
And I save filter settings
And I select the resetFilters option
Then I should see a Audi A3 in the results grid
