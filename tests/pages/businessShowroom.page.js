const NativePage = require('./native.page.js');
const businessShowroomPageData = require('../files/testData/businessShowroom.data.js');

class businessShowroomPage extends NativePage {

  get commonPageElement() {
    return this.getPage('common.locators');;
  }
  get desktopFiltersPageElement() {
    return this.getPage('desktopFilters.locators');
  }
  get quickFiltersPageElement() {
    return this.getPage('quickFilters.locators');;
  }

  openBusinessShowroomPage() {
    browser.url(businessShowroomPageData['url']);
    browser.setWindowSize(1920, 1080);
  }

  consentCookies() {
    const consentButton = $(this.commonPageElement.consentButton)
      browser.waitUntil(() => consentButton.isClickable())
      consentButton.click()
  }

  openDesktopFilter(filterKey) {
    const filterButton = $$('[data-key='+ filterKey + ']')[1]
    browser.waitUntil(() => filterButton.isClickable())
    filterButton.click()
  }

  optionDesktopFilter(filterKey) {
    const filterButton = $('[data-key='+ filterKey + ']')
    filterButton.click()
  }

  selectCheckbox(checkboxValue, checkboxName) {
    const filterCheckbox = $('#' + checkboxName.replace(/\s+/g, '-').replace(/^"|"$/g, '') + '-' +checkboxValue.replace(/\s+/g, '-').replace(/^"|"$/g, ''));
    filterCheckbox.click()
  }

  saveFilterSetting() {
    const saveFilterSettingButton = $(this.desktopFiltersPageElement.saveFilterSettingButton);
    saveFilterSettingButton.click()
  }

  setMonthlyPriceAndLeaseOption(minMonthlyPrice, maxMonthlyPrice, termAmount, kilometrageAmount) {
    browser.url(businessShowroomPageData['url']+'/?leaseOption[contractDuration]=' + termAmount + '&leaseOption[mileage]=' + kilometrageAmount + '&monthlyPrice='+ minMonthlyPrice +','+ maxMonthlyPrice);
  }

  validateNoResultsFoundMessage(partialMessage) {
    const noResultsFound = $(this.desktopFiltersPageElement.noResultsFound + '*=' + partialMessage)
    noResultsFound.waitForDisplayed();
  }

  validateTextFoundMessage(partialText) {
    const textFound = $(this.desktopFiltersPageElement.textFound + '*=' + partialText)
    textFound.waitForDisplayed();
  }

  validateMakeModelDisplayed(makeModel) {
    const makeModelFound = $('*=' + makeModel)
    makeModelFound.waitForDisplayed();
  }

  validatePriceDisplayed(monthlyPrice) {
      const monthlyPriceFound = $('*=' + monthlyPrice)
      monthlyPriceFound.waitForDisplayed();
    }
}

module.exports = businessShowroomPage;